<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Frontend.dashboard.home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/login', 'Auth\Logincontroller@showLogin')->name('auth.login');
Route::get('/register', 'Auth\RegisterController@showRegister')->name('auth.register');
Route::post('/processRegister', 'Auth\RegisterController@processRegister')->name('auth.processRegister');


Route::group(['as' => 'dashboard.', 'prefix' => 'dashboard', 'namespace' => 'Frontend'], function () {

    Route::get('/', 'DashboardController@Index')->name('dashboard');
    Route::get('/ads', 'DashboardController@Ads')->name('ads');
    Route::get('/pricing', 'DashboardController@Pricing')->name('pricing');
    Route::get('/contact', 'DashboardController@Contact')->name('contact');
    Route::get('/about_us', 'DashboardController@AboutUs')->name('about_us');
    Route::get('/product/{id}', 'DashboardController@product')->name('product');

});


Route::group(['as' => 'search.', 'prefix' => 'search', 'namespace' => 'Frontend'], function () {

	Route::post('/search', 'SerachController@Search')->name('search');

});

Route::group(['as' => 'checkout.', 'prefix' => 'checkout', 'namespace' => 'Frontend'], function () {
	
	Route::get('/checkout', 'OrderController@ShowForm')->name('checkout');

});