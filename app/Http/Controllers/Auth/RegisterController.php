<?php

namespace App\Http\Controllers\Auth;

use App\Model\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Auth\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }


    public function showRegister()
    {
        return view('auth.register');
    }

    public function processRegister()
    {
        $this->userValidation();

        $user = new User;

        //insert user data
        if(request()->role_id == 3)
        {
            $user->name     = request()->full_name;
            $user->email    = request()->email;
            $user->phone    = request()->phone;
            $user->password = Hash::make(request()->password);
            $user->role_id  = request()->role_id;

            $user->save();

        }else if(request()->role_id == 2){

            $user->name     = request()->full_name;
            $user->email    = request()->email;
            $user->dob      = request()->dob;
            $user->phone    = request()->phone;
            $user->password = Hash::make(request()->password);
            $user->tin_no   = request()->tin_no;
            $user->role_id  = request()->role_id;

            $user->save();
        }

        $cmsTable = array(
            'name'     => request()->full_name,
            'photo'    => 'uploads/1/2020-03/avatar.png',
            'email'    => request()->email,
            'password' => Hash::make(request()->password),
            'status'   => 'Active',
            'id_cms_privileges' => request()->role_id
        );

        DB::table('cms_users')->insert($cmsTable);
        

        return redirect()->back();
    }

    public function userValidation()
    {
        return request()->validate([
            'full_name' => 'required',
            'email' => 'required|unique:users,email',
            'phone' => 'required|unique:users,phone',
            'password' => 'required|min:6'
        ]);
    }
}
