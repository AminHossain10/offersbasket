<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class SerachController extends Controller
{
    public function Search()
    {
    	$keyword = request()->keyword;

    	$tags = DB::table('product_tags')
    			->select('id')
    			->where('tag_name', 'like', '%'.$keyword.'%')
    			->orWhere('slug', 'like', '%'.$keyword.'%')
    			->first();

    	$product = DB::table('products')->select('*')
    				->where('tag_id', '=', $tags->id)
    				->orWhere('pro_name', 'like', "%".$keyword."%")
    				->orWhere('discount', 'like', "%".$keyword."%")
    				->get();

    	return response()->json(['success' => true, 'data' => $product]);
    }
}
