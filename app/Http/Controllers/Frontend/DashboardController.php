<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Db;

class DashboardController extends Controller
{
    public function Index()
    {
        $categories = DB::table('shop_categories')->get();
        $product    = DB::table('products')->get();
    	return view('Frontend.dashboard.home', ['product' => $product, 'category' => $categories]);
    }


    public function Ads()
    {
        $categories = DB::table('shop_categories')->get();
        $product    = DB::table('products')->get();
        $shop = DB::table('shops')->get();

    	return view('Frontend.dashboard.ads', ['shops' => $shop, 'product' => $product, 'category' => $categories]);
    }


    public function Contact()
    {
    	return view('Frontend.dashboard.contact');
    }


    public function Pricing()
    {
    	return view('Frontend.dashboard.pricing');
    }


    public function AboutUs()
    {
    	return view('Frontend.dashboard.about_us');
    }

    public function product($id){

        $product = DB::table('products')->where('id', $id)->first();
        $catrgories = DB::table('shop_categories')->get();

        return view('Frontend.dashboard.product', ['product' => $product, 'category' => $catrgories]);
    }
}
