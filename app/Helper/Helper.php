<?php

use Illuminate\Support\Facades\DB;

function GetStreet($bid){

	$street = DB::table('branches')->select('*')->where('id', $bid)->first();
	// dd($street, $bid);
	return $street;
}


function GetProducts($shopid)
{
	$branch = DB::table('branches')->where('shop_id', $shopid)->first();
	$pro = DB::table('products')->where('barnch_id', $branch->id)->get();

	return $pro;
}