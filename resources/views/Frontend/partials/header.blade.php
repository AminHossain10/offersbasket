<header class="site-navbar container py-0 " role="banner">

      <!-- <div class="container"> -->
        <div class="row align-items-center">
          
          <div class="col-6 col-xl-2">
            <h1 class="mb-0 site-logo"><a href="{{ route('dashboard.dashboard') }}" class="mb-0">OfferBasket</a></h1>
          </div>
          <div class="col-12 col-md-10 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right" role="navigation">

            <ul class="site-menu js-clone-nav mr-auto d-none d-lg-block">
              <li class="active"><a href="{{ route('dashboard.dashboard') }}">Home</a></li>
              <li><a href="{{ route('dashboard.ads') }}">Ads</a></li>
              <li><a href="{{ route('dashboard.pricing') }}">Pricing</a></li>
              <li><a href="{{ route('dashboard.contact') }}">Contact</a></li>
              <li class="has-children">
                <a href="#">About</a>
                <ul class="dropdown">
                  <li><a href="{{ route('dashboard.about_us') }}">The Company</a></li>
                  <li><a href="#">Careers</a></li>
                </ul>
              </li>

              <li class="has-children">
                <a href="#">Join</a>
                <ul class="dropdown">
                  <li><a href="{{ route('auth.login') }}">Login</a></li>
                  <li><a href="{{ route('auth.register') }}">Register</a></li>
                </ul>
              </li>
            </ul>

            </nav>
          </div>

          <div class="d-inline-block d-xl-none ml-auto py-3 col-6 text-right" style="position: relative; top: 3px;">
            <a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a>
          </div>

        </div>
      <!-- </div> -->
      
    </header>