@extends('Frontend.partials.main')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/css/logsign.css') }}">
@endsection

@section('content')

<div class="container register">
  <div class="row">

    <div class="col-md-3 register-left">
        <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt="" />
        <h3>Welcome</h3>
        <p>Now you are a member. Login to your account</p>
        <a href="{{ route('auth.register') }}"> <button class="btnLogin">Register</button></a><br/>
    </div>

    <div class="col-md-9 register-right">
      <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
          <h3 class="register-heading">Login Account</h3>
          <div class="row register-form justify-content-center align-items-center">
            <div class="col-md-12">
              <form action="{{ route('postLogin') }}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                <div class="form-group">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email*" required/>

                    <div class="col-md-6">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password*" required/>

                    <div class="col-md-6">
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <input type="submit" class="btnLogin" style="background: #f89d13;" value="Login" />

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
