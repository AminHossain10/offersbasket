
@extends('Frontend.partials.main')

@section('styles')
    <link rel="stylesheet" href="{!! asset('/css/logsign.css') !!}">
@endsection

@section('content')

<div class="container register">

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        <div class="col-md-3 register-left">
            <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/>
            <h3>Welcome</h3>
            <p>You are 30 seconds away to become a member!</p>
            <a href="{{ route('auth.login') }}"> <button class="btnLogin">Login</button></a><br/>
        </div>

        <div class="col-md-9 register-right">

            <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active " id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Buyer</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Seller</a>
                </li>
            </ul>

            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <h3 class="register-heading">Create Account</h3>
                    <form action="{{ route('auth.processRegister') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row register-form">
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Full Name *" name="full_name" value="" required/>
                            </div>
                            
                            <div class="form-group">
                                <input type="text" minlength="11" maxlength="11" name="phone" class="form-control" placeholder="Phone *" value="" />
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email *" name="email" value="" required/>
                            </div>

                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Password *" name="password" value="" required/>
                            </div>
                            
                            <input type="hidden" placeholder="*" name="role_id" value="3"/>
                            <input type="submit" class="btnRegister"  value="Register"/>
                        </div>
                        
                    </div>
                    </form>
                </div>

                <div class="tab-pane fade show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <h3  class="register-heading">Create Account</h3>
                     <form action="{{ route('auth.processRegister') }}" method="POST" enctype="multipart/form-data">
                     @csrf
                    <div class="row register-form">
                       
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Full Name *" name="full_name" value="" required/>
                            </div>

                             <div class="form-group">
                                <input type="date" class="form-control" placeholder="*" name="dob" value="" />
                            </div>

                            <div class="form-group">
                                <input type="text" minlength="11" maxlength="11" name="phone" class="form-control" placeholder="Phone *" value="" />
                            </div>
                               
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email *" name="email" value="" required/>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Password *" name="password" value="" required/>
                            </div>
                           
                            <div class="form-group">
                                <input type="text" name="tin_no" class="form-control" placeholder="TIN No. *" value="" />
                            </div>

                            <input type="hidden" placeholder="*" name="role_id" value="2"/>
                            <input type="submit" class="btnRegister"  value="Register"/>
                        </div>
                        
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
